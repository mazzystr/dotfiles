" BEGIN PERL
autocmd FileType perl set showmatch
autocmd FileType perl set makeprg=perl\ -c\ %\ $*
autocmd FileType perl set errorformat=%f:%l:%m
autocmd FileType perl set autowrite
let perl_extended_vars = 1
let perl_sync_dist     = 250


autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif

augroup JumpCursorOnEdit
   au!
   autocmd BufReadPost *
            \ if expand("<afile>:p:h") !=? $TEMP |
            \   if line("'\"") > 1 && line("'\"") <= line("$") |
            \     let JumpCursorOnEdit_foo = line("'\"") |
            \     let b:doopenfold = 1 |
            \     if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
            \        let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
            \        let b:doopenfold = 2 |
            \     endif |
            \     exe JumpCursorOnEdit_foo |
            \   endif |
            \ endif
   " Need to postpone using "zv" until after reading the modelines.
   autocmd BufWinEnter *
            \ if exists("b:doopenfold") |
            \   exe "normal zv" |
            \   if(b:doopenfold > 1) |
            \       exe  "+".1 |
            \   endif |
            \   unlet b:doopenfold |
            \ endif
augroup END

let g:vim_json_syntax_conceal = 0

set nocompatible
set nobackup

set showcmd

set foldmethod=marker

set bg=light
filetype on
filetype plugin on
syntax enable
set grepprg=grep\ -nH\ $*

set expandtab
set autoindent
set smartindent
set number
set smarttab
set shiftwidth=2
set softtabstop=2

set wildmenu
set wildmode=list:longest,full

set hlsearch

let g:clipbrdDefaultReg = '+'

syntax on

set foldmethod=indent
set foldnestmax=10
set nofoldenable

set foldlevel=1
nmap <F2> 0v/{<CR>%zf

set pastetoggle=<F12>
nmap <F11> :set nu!<CR>
nmap <F10> :set syntax=asciidoc<CR>
