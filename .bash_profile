# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# Source local definitions
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi

if [ ! -d ~/.history ]; then mkdir ~/.history; fi
HISTSIZE=9999
HISTFILE=~/.history/history$(tty | tr '/' '-')

# set vi bindings ~ Press esc to enable
set -o vi

# Load minikube completion
if [ `which minikube` ]; then
  source <(minikube completion bash)
fi

# Load kubectl completion
if [ `which kubectl` ]; then
  source <(kubectl completion bash)
fi

#Darwin stuff
if [ $(uname -s) == 'Darwin' ]; then
  export BASH_SILENCE_DEPRECATION_WARNING=1
  export ECHO="echo"
  export PROMPT_COLOR_BEGIN="\033"
  export PROMPT_COLOR_END="\033[0m"
  export PROMPT_COLOR_HOST="[1;36m"

  #Make my mac fast
  #defaults write com.apple.Firefox WebKitInitialTimedLayoutDelay 0.1
  #defaults write NSGlobalDomain KeyRepeat -int 0

  # Load brew bash completion
  [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

  # Load kubectl completion
  #if [ `which aws_completer` ]; then
  #  source <(aws_completer )
  #fi

  alias gameon='\
defaults write NSGlobalDomain _HIHideMenuBar -bool true; \
defaults write com.apple.Dock autohide-delay -float 200; \
defaults write com.apple.dock autohide-delay -float 200; \
defaults write com.apple.dock autohide-time-modifier -float 200; \
killall Dock; \
'
  alias gameoff='\
defaults write NSGlobalDomain _HIHideMenuBar -bool false; \
defaults write com.apple.dock autohide-delay -int 0; \
defaults write com.apple.dock autohide-time-modifier -float 0.4; \
killall Dock'
  osxwifii() {
    sudo /System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport -z
    openssl rand -hex 6 | sed "s/\(..\)/\1:/g; s/.$//" | xargs sudo ifconfig en0 ether
    networksetup -detectnewhardware
  }
  alias osxwifi='osxwifii'
  osxwifibakk() {
    sudo /System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport -z
    sudo ifconfig en0 ether 6c:40:08:c1:4a:14
    networksetup -detectnewhardware
  }
  alias osxwifibak='osxwifibakk'
  alias osxnf="osascript -e 'tell app \"Finder\" to make new Finder window'"
  alias kinit='kinit ccallega@REDHAT.COM'
  alias ll='ls -la -G'
  alias spyder='nohup ~/anaconda3/bin/spyder --new-instance > /dev/null 2>&1 &'

  alias vboxstartrhel8='VBoxManage startvm --type separate rhel8'
  alias vboxstartfedora='VBoxManage startvm --type separate fedora'
  alias vboxmountgit='sudo mount -o uid=$(id -u ${LOGNAME}),gid=$(id -u ${LOGNAME}) -t vboxsf git ~/git'

  ######################################################################
  # Red Hat VPN
  vpnkillalll() {
    sudo kill -9 $(ps -ef | grep -v grep | grep -i tunnel | awk {'print $2'})
  }
  alias vpnphx2='osascript -e "tell application \"/Applications/Tunnelblick.app\"" -e "connect \"redhat-phx2\"" -e "end tell" > /dev/null 2>&1'
  alias novpnphx2='osascript -e "tell application \"/Applications/Tunnelblick.app\"" -e "disconnect \"redhat-phx2\"" -e "end tell" > /dev/null 2>&1'
  alias vpnrdu2='osascript -e "tell application \"/Applications/Tunnelblick.app\"" -e "connect \"redhat-rdu2\"" -e "end tell" > /dev/null 2>&1'
  alias novpnrdu2='osascript -e "tell application \"/Applications/Tunnelblick.app\"" -e "disconnect \"redhat-rdu2\"" -e "end tell" > /dev/null 2>&1'
  alias vpnkillall='vpnkillalll'

  ######################################################################
  # Brew path
  export PATH="/usr/local/{,s}bin:$PATH"

  # added by Anaconda3 4.4.0 installer
  export PATH="$HOME/anaconda/bin:$PATH"

  # added by Anaconda3 installer
  export PATH="$HOME/anaconda3/bin:$PATH"

  ######################################################################
  # Enable golang environment
  golangenvv() {
    export GOROOT=/Users/go
    export GOPATH=$HOME/go
    export PATH=$GOROOT/bin:$GOPATH/bin:$PATH
    cd $GOPATH/src
  }
  alias setgolangenv='golangenvv'

fi

# Linux stuff
if [ $(uname -s) == 'Linux' ]; then
  export ECHO="echo -e"
  export PROMPT_COLOR_BEGIN="\e"
  export PROMPT_COLOR_END="\e[0m"
  if [[ $(uname -n) =~ 'rhel' ]]; then
    export PROMPT_COLOR_HOST="[1;42;41m"
  elif [[ $(uname -n) =~ 'fedora' ]]; then
    export PROMPT_COLOR_HOST="[1;42;44m"
  else echo; fi

  source /usr/bin/aws_bash_completer

  alias setdate='sudo chronyc makestep'
  alias mountgit='sudo mount -o uid=$(id -u ${LOGNAME}),gid=$(id -u ${LOGNAME}) -t vboxsf git ~/git'
  alias mountgo='sudo mount -o uid=$(id -u ${LOGNAME}),gid=$(id -u ${LOGNAME}) -t vboxsf go ~/go'
  alias vboxinstallheaders='sudo umount /mnt 2> /dev/null; sudo mount /dev/cdrom /mnt > /dev/null 2>&1 && file /mnt/VBoxLinuxAdditions.run > /dev/null 2>&1;
      if [ $? == 0 ]; then
        sudo dnf install -y binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms qt5-qtx11extras libxkbcommon libxcrypt-compat;
        export KERN_DIR=/usr/src/kernels/`uname -r`;
        cd /mnt && sudo ./VBoxLinuxAdditions.run;
  fi'
  ######################################################################
  # Enable golang environment
  golangenvv() {
    export GOROOT=/home/go
    export GOPATH=$HOME/go
    export PATH=$GOROOT/bin:$GOPATH/bin:$PATH
    cd $GOPATH/src
  }
  alias setgolangenv='golangenvv'
fi

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# Set kerberos config
export KRB5_CONFIG='~/.krb5/krb5.conf'

# Start ssh-agent functions
if ! $(pgrep -U $UID ssh-agent > /dev/null); then
  export SSHAUTHSOCK=$HOME/.ssh/.agent
  pkill ssh-agent
  mkdir $(dirname $SSHAUTHSOCK) 2> /dev/null
  rm -rf $SSHAUTHSOCK  2> /dev/null
  for i in SSH_AGENT_PID SSH_AUTH_SOCK SSH_CLIENT SSH_CONNECTION SSH_TTY; do
    unset $i
  done
  export sshagent=$(nohup ssh-agent -s -a ${SSHAUTHSOCK} &)
  export sshauthsock=$(echo ${sshagent} | awk -F'; ' {'print $1'})
  export sshagentpid=$(echo ${sshagent} | awk -F'; ' {'print $3'})
  export ${sshauthsock}
  export ${sshagentpid}
  for i in sshagent sshauthsock sshagentpid; do
    unset $i
  done
else
  mkdir ~/.ssh 2> /dev/null
  export SSH_AGENT_PID=$(pgrep -f ssh-agent)
  export SSH_AUTH_SOCK=~/.ssh/.agent
fi

# Add ssh keys to agent
for i in $(find ~/.ssh -name "*.pub"); do
  j=($(cat $i))
  if ! $(ssh-add -L | grep ${j[1]} > /dev/null); then
    if [ $(uname -s) == 'Darwin' ]; then
      security unlock-keychain
      ssh-add -K $(echo $i | sed 's/.pub$//')
    else
      ssh-add $(echo $i | sed 's/.pub$//')
    fi
  fi
done

# Start gpg-agent functions (vimgpg)
if [ ! ${GPG_AGENT_INFO} ]; then
  if [ $(pgrep gpg-agent | tr "\r\n" " " | grep -o " " | wc -l) != "1" ]; then
    pkill gpg-agent > /dev/null 2>&1
    eval $(gpg-agent --daemon --enable-ssh-support)
  fi
  export GPG_AGENT_INFO="$HOME/.gnupg/S.gpg-agent,$(pgrep gpg-agent),1"
  export GPG_TTY=$(tty)
  #export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
fi

# Functions
gdifff() {
  git diff $1
}
gbrrmm() {
  I=main; git checkout $I; git pull; git push origin --delete $1 2> /dev/null; git branch -D $1; git fetch --all --prune
}
gbrcrr() {
  I=main; git checkout $I; git pull; git branch $1; git checkout $1; git push --set-upstream origin $1
}
gcoo() {
  git checkout $1
}
gsyncdefaultt() {
  I=main; git checkout $I; git fetch upstream; git reset --hard upstream/$I; git push origin $I -f
}
getvenvv() {
  if [ ! -d "$HOME/virtualenvs" ]; then mkdir ~/virtualenvs; fi
  basename $(ls -d ~/virtualenvs/*)
}
setvenvv() {
  if [ $1 ]; then
    echo "Setting python virtual env: $1"
    source ~/virtualenvs/$1/bin/activate
    cd ~/virtualenvs/$1
  else
    echo "Please enter an arg along with command"
    echo -n "Possible args are: "; echo $(basename $(ls -d ~/virtualenvs/*))
  fi
}

# Aliases
alias gcd='cd $HOME/Downloads/git'
alias gdiff='gdifff'
alias gdoc='cat ~/Documents/gitfunctions'
alias gbrco='gcoo'
alias gbrcr='gbrcrr'
alias gbrls='git branch -l; git branch -r'
alias gbrrm='gbrrmm'
alias gco='gcoo'
alias gst='git status'
alias gsyncdefault='gsyncdefaultt'
alias getvenv='getvenvv'
alias setvenv='setvenvv'

# Syntactic sugar for ANSI escape sequences
txtblk='\e[0;30m' # Black - Regular
txtred='\e[0;31m' # Red
txtgrn='\e[0;32m' # Green
txtylw='\e[0;33m' # Yellow
txtblu='\e[0;34m' # Blue
txtpur='\e[0;35m' # Purple
txtcyn='\e[0;36m' # Cyan
txtwht='\e[0;37m' # White
bldblk='\e[1;30m' # Black - Bold
bldred='\e[1;31m' # Red
bldgrn='\e[1;32m' # Green
bldylw='\e[1;33m' # Yellow
bldblu='\e[1;34m' # Blue
bldpur='\e[1;35m' # Purple
bldcyn='\e[1;36m' # Cyan
bldwht='\e[1;37m' # White
unkblk='\e[4;30m' # Black - Underline
undred='\e[4;31m' # Red
undgrn='\e[4;32m' # Green
undylw='\e[4;33m' # Yellow
undblu='\e[4;34m' # Blue
undpur='\e[4;35m' # Purple
undcyn='\e[4;36m' # Cyan
undwht='\e[4;37m' # White
bakblk='\e[40m'   # Black - Background
bakred='\e[41m'   # Red
badgrn='\e[42m'   # Green
bakylw='\e[43m'   # Yellow
bakblu='\e[44m'   # Blue
bakpur='\e[45m'   # Purple
bakcyn='\e[46m'   # Cyan
bakwht='\e[47m'   # White
txtrst='\e[0m'    # Text Reset

export PS1_HOSTNAME="\
[${PROMPT_COLOR_BEGIN}\
${PROMPT_COLOR_HOST}\
$(hostname)\
${PROMPT_COLOR_END}]"\

FONT='\[\e[0;37m\]'
COLOR_END="\[\e\]"
USER_COLOR_BEGIN="\[\e[0;36m\]"
DIR_COLOR_BEGIN="\[\e[0;34m\]"
PS1="[${USER_COLOR_BEGIN}\u${COLOR_END}${FONT}@${DIR_COLOR_BEGIN}\w${COLOR_END}${FONT}]\$ "

#Set hostname/date/git
PROMPT_COMMAND="~/.bash_profile_cmd.sh"

# Android environment
export JAVA_HOME='/Library/Java/JavaVirtualMachines/jdk-10.0.2.jdk/Contents/Home/'
export ANDROID_HOME='/usr/local/Caskroom/android-sdk/4333796//tools/'
export ANDROID_NDK_HOME='~/Library/Android/sdk/ndk-bundle'
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
