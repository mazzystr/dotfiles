#!/bin/sh

#Set date color
export PROMPT_COLOR_DATE="[1;33m"
export DATE_PROMPT="[\
${PROMPT_COLOR_BEGIN}\
${PROMPT_COLOR_DATE}\
$(date +'%I:%M:%S %p')\
${PROMPT_COLOR_END}\
]"

#set git branch color
if [[ ${HOSTNAME} == *"ccallegar-mbpr"* ]]; then
  export PROMPT_COLOR_GIT="[1;41m"
  export GIT_PS1_SHOWDIRTYSTATE=yes
  source ~/.git_prompt.sh
  export GIT_PROMPT="\
${PROMPT_COLOR_BEGIN}\
${PROMPT_COLOR_GIT}\
$(__git_ps1 'GIT: On branch %s')\
${PROMPT_COLOR_END}"
else
  export GIT_PROMPT=""
fi

# set cmd output color
${ECHO} "${PS1_HOSTNAME} ${DATE_PROMPT} ${GIT_PROMPT}"
